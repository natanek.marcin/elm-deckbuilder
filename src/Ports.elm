port module Ports exposing (storeValue)


type alias Key =
    String


type alias Value =
    String


storeValue : Key -> Value -> Cmd msg
storeValue key value =
    storeValuePort ( key, value )


port storeValuePort : ( Key, Value ) -> Cmd msg
