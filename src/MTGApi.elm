module MTGApi exposing (Card, CardColor(..), Msg(..), cardsDecoder, getImg, searchCardByName)

import Constants exposing (..)
import Http
import Json.Decode exposing (Decoder, at, fail, field, int, list, maybe, string, succeed)
import Json.Decode.Pipeline exposing (..)
import Json.Encode as Encode
import String exposing (..)
import Url.Builder


type Msg
    = GotCards String (Result Http.Error String)


apiURL =
    "https://api.scryfall.com"


searchCardByName : String -> Cmd Msg
searchCardByName name =
    let
        query =
            [ Url.Builder.string "q" name ]

        url =
            Url.Builder.crossOrigin apiURL [ "cards", "search" ] query
    in
    Http.request
        { method = "GET"
        , headers = []
        , url = url
        , body = Http.emptyBody
        , expect = Http.expectString (GotCards name)
        , timeout = Nothing
        , tracker = Nothing
        }


type alias Card =
    { name : String
    , cmc : Int
    , colors : List CardColor
    , types : String
    , rarity : String
    , set : String
    , text : Maybe String
    , power : Maybe String
    , toughness : Maybe String
    , artImage : Maybe String
    , cardImage : Maybe String
    }


type CardColor
    = White
    | Black
    | Red
    | Blue
    | Green


getImg : Card -> String
getImg card =
    let
        cardImage =
            card.artImage
                |> Maybe.withDefault (Maybe.withDefault defaultCardFace card.cardImage)
    in
    if cardImage == "unknown" then
        defaultCardFace

    else
        cardImage



--- Decoders ---


cardsDecoder : Json.Decode.Decoder (List Card)
cardsDecoder =
    let
        cardDecoder : Json.Decode.Decoder Card
        cardDecoder =
            Json.Decode.succeed Card
                |> required "name" string
                |> required "cmc" int
                |> custom colorsDecoder
                |> required "type_line" string
                |> required "rarity" string
                |> required "set" string
                |> custom (maybe (field "oracle_text" string))
                |> custom (maybe (field "power" string))
                |> custom (maybe (field "toughness" string))
                |> custom (maybe (at [ "image_uris", "art_crop" ] string))
                |> custom (maybe (at [ "image_uris", "small" ] string))
    in
    at [ "data" ] (list cardDecoder)


colorsDecoder : Json.Decode.Decoder (List CardColor)
colorsDecoder =
    field "color_identity" (list (string |> Json.Decode.andThen colorDecoder))


colorDecoder : String -> Json.Decode.Decoder CardColor
colorDecoder color =
    let
        lccolor =
            toLower color
    in
    if List.member lccolor [ "white", "w" ] then
        succeed White

    else if List.member lccolor [ "green", "g" ] then
        succeed Green

    else if List.member lccolor [ "blue", "u" ] then
        succeed Blue

    else if List.member lccolor [ "black", "b" ] then
        succeed Black

    else if List.member lccolor [ "red", "r" ] then
        succeed Red

    else
        fail ("Could not recognize card color: " ++ lccolor)
