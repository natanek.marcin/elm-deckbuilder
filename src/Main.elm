module Main exposing (Model, Msg(..), init, main, update, view)

import Browser
import Browser.Navigation as Nav
import Constants exposing (..)
import Debouncer.Messages as Debouncer exposing (Debouncer, fromSeconds, provideInput, settleWhenQuietFor, toDebouncer)
import Debug exposing (..)
import Deck exposing (..)
import Dict exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode
import MTGApi
import Platform.Cmd
import Ports exposing (..)
import RemoteData exposing (..)
import Task
import Time
import Url
import Url.Parser exposing ((</>), Parser, int, map, oneOf, parse, s, string, top)


main : Program () Model Msg
main =
    Browser.application
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }



---- MODEL ----


type alias Model =
    { key : Nav.Key
    , url : Url.Url
    , route : Route
    , getCardsDebouncer : Debouncer Msg
    , searchInput : String
    , prevCards : List MTGApi.Card
    , foundCards : WebData (List MTGApi.Card)
    , deck : Deck
    , notification : Maybe String
    }


type Route
    = Deckbuilder
    | Err404
    | About


init : flags -> Url.Url -> Nav.Key -> ( Model, Platform.Cmd.Cmd Msg )
init flags url key =
    ( { key = key
      , url = url
      , route = Deckbuilder
      , getCardsDebouncer =
            Debouncer.manual
                |> settleWhenQuietFor (Just <| fromSeconds 0.5)
                |> toDebouncer
      , searchInput = ""
      , prevCards = []
      , foundCards = NotAsked
      , deck = Dict.empty
      , notification = Nothing
      }
    , Cmd.none
    )



---- UPDATE ----


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url
    | UpdateSearchText String
    | GetCardsDebouncer (Debouncer.Msg Msg)
    | GetCards String
    | MTGApiMsg MTGApi.Msg
    | AddToDeck MTGApi.Card
    | RemoveFromDeck MTGApi.Card


updateDebouncer : Debouncer.UpdateConfig Msg Model
updateDebouncer =
    { mapMsg = GetCardsDebouncer
    , getDebouncer = .getCardsDebouncer
    , setDebouncer = \debouncer model -> { model | getCardsDebouncer = debouncer }
    }


perform : msg -> Cmd msg
perform =
    Task.perform identity << Task.succeed


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    ( model, Nav.pushUrl model.key (Url.toString url) )

                Browser.External href ->
                    ( model, Nav.load href )

        UrlChanged url ->
            ( { model | url = url, route = parseRoute url }, Cmd.none )

        UpdateSearchText val ->
            update (GetCards val |> provideInput |> GetCardsDebouncer) model
                |> Tuple.mapFirst
                    (\m ->
                        { m
                            | searchInput = val
                            , prevCards = RemoteData.withDefault model.prevCards model.foundCards
                            , foundCards = Loading
                        }
                    )

        GetCardsDebouncer debMsg ->
            Debouncer.update update updateDebouncer debMsg model

        GetCards val ->
            ( { model | searchInput = val }
            , Cmd.map MTGApiMsg (MTGApi.searchCardByName val)
            )

        MTGApiMsg apimsg ->
            case apimsg of
                MTGApi.GotCards query (Ok result) ->
                    case Json.Decode.decodeString MTGApi.cardsDecoder result of
                        Ok gotCards ->
                            ( { model | foundCards = Success gotCards }
                            , Ports.storeValue ("request_" ++ query) result
                            )

                        Err err ->
                            ( { model | notification = Just ("Parsing failed: " ++ toString err) }
                            , Cmd.none
                            )

                MTGApi.GotCards query (Err result) ->
                    ( { model | foundCards = Failure result }, Cmd.none )

        AddToDeck card ->
            case addCard model.deck card of
                Ok deck ->
                    ( { model | deck = deck }, saveDeck deck )

                Err err ->
                    ( { model | notification = Just (toString err) }, Cmd.none )

        RemoveFromDeck card ->
            case removeCard model.deck card of
                Ok deck ->
                    ( { model | deck = deck }, saveDeck deck )

                Err err ->
                    ( { model | notification = Just (toString err) }, Cmd.none )


routeParser : Parser (Route -> a) a
routeParser =
    oneOf
        [ Url.Parser.map Deckbuilder top
        , Url.Parser.map About (Url.Parser.s "About")
        ]


parseRoute : Url.Url -> Route
parseRoute url =
    parse routeParser url
        |> Maybe.withDefault Err404


getRouteInfo : Route -> { title : String, url : String }
getRouteInfo route =
    case route of
        Deckbuilder ->
            { title = "Deckbuilder", url = "/" }

        Err404 ->
            { title = "Not Found", url = "/404" }

        About ->
            { title = "About", url = "/About" }



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



---- VIEW ----


view : Model -> Browser.Document Msg
view model =
    { title = "MTG Deckbuilder"
    , body =
        [ renderMenu model
        , renderRoute model
        ]
    }


renderMenu : Model -> Html Msg
renderMenu model =
    div [ class "ui pointing menu" ]
        (List.map
            (renderMenuItem model.route)
            [ Deckbuilder, About ]
        )


renderMenuItem currentRoute targetRoute =
    let
        routeInfo =
            getRouteInfo targetRoute
    in
    a
        [ classList [ ( "active", currentRoute == targetRoute ) ]
        , class "item"
        , href routeInfo.url
        ]
        [ text routeInfo.title ]


renderRoute : Model -> Html Msg
renderRoute model =
    case model.route of
        Deckbuilder ->
            renderDeckbuilder model

        About ->
            p [] [ text "TODO about page" ]

        Err404 ->
            p [] [ text "Page not found" ]


renderDeckbuilder : Model -> Html Msg
renderDeckbuilder model =
    div []
        [ displayNotification model.notification
        , div
            [ class "ui icon input", classList [ ( "loading", isLoading model.foundCards ) ] ]
            [ input
                [ type_ "text"
                , placeholder "Search cards..."
                , value model.searchInput
                , onInput UpdateSearchText
                ]
                []
            , i [ class "search icon" ] []
            ]
        , div [ class "ui divider" ] []
        , div [ class "ui two column divided grid" ]
            [ div [ class "row" ]
                [ div [ class "ten wide column" ] [ displayCards model ]
                , div [ class "six wide column" ] [ displayDeck model.deck ]
                ]
            ]
        ]


displayCards : Model -> Html Msg
displayCards model =
    let
        disp =
            case model.foundCards of
                NotAsked ->
                    text "Use the search to find cards."

                Loading ->
                    div []
                        [ div [ class "ui active inverted dimmer" ]
                            [ div [ class "ui active loader" ] []
                            ]
                        , div []
                            [ div [ class "ui link centered cards" ] (List.map (displayCardDetails AddToDeck) model.prevCards)
                            ]
                        ]

                Success cards ->
                    case cards of
                        [] ->
                            div [ class "ui" ] [ text "Nothing found." ]

                        _ ->
                            div [ class "ui link centered cards" ] (List.map (displayCardDetails AddToDeck) cards)

                Failure err ->
                    div [] [ text ("Error occured while downloading cards: " ++ toString err) ]
    in
    div []
        [ p [] [ text "Search results" ]
        , disp
        ]


renderManaCurve : ManaCurve -> Html Msg
renderManaCurve curve =
    div [ class "ui segment" ]
        [ text "Mana Curve"
        , div [ class "ui progress" ]
            (List.range 0 6
                |> List.map (displayCurveBar curve)
            )
        ]


displayCurveBar : ManaCurve -> Int -> Html Msg
displayCurveBar curve cmc =
    let
        count =
            Dict.get cmc curve.curve
                |> Maybe.withDefault 0

        percent =
            toString (100 * (count // curve.biggestCmc))

        displayCmc toDisplay =
            case toDisplay of
                6 ->
                    "6+"

                i ->
                    toString i
    in
    div
        [ class "bar"
        , style "width" (percent ++ "%")
        , style "margin" "2px"
        , Html.Attributes.attribute "data-tooltip" ("CMC: " ++ displayCmc cmc)
        , Html.Attributes.attribute "data-position" "left center"
        ]
        [ div [ class "progress" ] [ text (toString count) ]
        ]


displayDeck : Deck -> Html Msg
displayDeck deck =
    div []
        [ p [] [ text <| "Your deck (" ++ toString (getCardCount deck) ++ ")" ]
        , renderManaCurve (getManaCurve deck)
        , div [ class "ui link centered cards" ]
            (Dict.values deck
                |> List.sortBy (\e -> ( e.card.cmc, e.card.name ))
                |> List.map
                    (\e ->
                        div []
                            [ displayCardDetails RemoveFromDeck e.card
                            , p [] [ text (toString e.count) ]
                            ]
                    )
            )
        ]


displayNotification : Maybe String -> Html Msg
displayNotification notif =
    case notif of
        Just str ->
            div [ class "ui warning message" ] [ text str ]

        Nothing ->
            div [] []


displayCardImage : MTGApi.Card -> Html Msg
displayCardImage card =
    div
        [ class "ui card" ]
        [ div [ class "ui fluid image" ]
            [ img [ src (Maybe.withDefault defaultCardFace card.cardImage) ] []
            ]
        ]


displayCardDetails : (MTGApi.Card -> Msg) -> MTGApi.Card -> Html Msg
displayCardDetails action card =
    div [ class "ui card", onClick <| action card ]
        [ div [ style "padding" "10px" ]
            [ div [ class "right floated" ] [ text (toString card.cmc) ] ]
        , div [ class "ui fluid image" ]
            [ img
                [ src (MTGApi.getImg card)
                , style "object-fit" "cover"
                , style "height" "230px"
                ]
                []
            ]
        , div [ class "content" ]
            [ div [ class "header" ] [ text card.name ]
            , div [ class "meta" ] [ text card.types ]
            , div [ class "description" ] [ text (Maybe.withDefault "" card.text) ]
            ]
        , displayCardExtras card
        ]


displayCardExtras : MTGApi.Card -> Html Msg
displayCardExtras card =
    case ( card.power, card.toughness ) of
        ( Just p, Just t ) ->
            div [ class "extra content" ]
                [ div [ class "right floated" ] [ text (p ++ "/" ++ t) ]
                ]

        ( _, _ ) ->
            div [] []
