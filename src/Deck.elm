module Deck exposing (CardId, Deck, DeckEntry, DeckError, ManaCurve, addCard, getCardCount, getManaCurve, removeCard, saveDeck)

import Dict exposing (Dict)
import Dict.Extra
import Json.Encode as Encode
import List.Extra
import MTGApi
import Ports


type alias Deck =
    Dict CardId DeckEntry


type alias CardId =
    String


type alias DeckEntry =
    { card : MTGApi.Card
    , count : Int
    }


type DeckError
    = TooManyCards
    | NegativeCards
    | TooManyCardInstances CardId
    | NegativeCardInstances CardId


type alias ManaCurve =
    { targetTotalCards : Int
    , biggestCmc : Int
    , curve : Dict Int Int
    }


addCard : Deck -> MTGApi.Card -> Result DeckError Deck
addCard deck card =
    makeCardOperator ((+) 1) deck card


removeCard : Deck -> MTGApi.Card -> Result DeckError Deck
removeCard deck card =
    makeCardOperator (\x -> x - 1) deck card


getCardId : MTGApi.Card -> CardId
getCardId card =
    card.name


getCardCount : Deck -> Int
getCardCount deck =
    Dict.values deck
        |> List.map .count
        |> List.sum


isLand : MTGApi.Card -> Bool
isLand entry =
    entry
        |> .types
        |> String.contains "Land"


checkAddConstraints : (Int -> Int) -> MTGApi.Card -> Deck -> Result DeckError Deck
checkAddConstraints operator card deck =
    let
        cardId =
            getCardId card

        entry =
            Dict.get cardId deck

        targetCount =
            entry
                |> Maybe.map .count
                |> Maybe.withDefault 0
                |> operator

        targetTotalCards =
            deck
                |> getCardCount
                |> operator
    in
    if targetTotalCards > 250 then
        Err TooManyCards

    else if targetTotalCards < 0 then
        Err NegativeCards

    else if targetCount > 4 && not (isLand card) then
        Err <| TooManyCardInstances cardId

    else if targetCount < 0 then
        Err <| NegativeCardInstances cardId

    else
        Ok deck


isEmpty : DeckEntry -> Bool
isEmpty entry =
    entry.count == 0


makeEntryModifier : (Int -> Int) -> MTGApi.Card -> (Deck -> Deck)
makeEntryModifier operator card =
    let
        applyOperation entry =
            entry
                |> Maybe.map (\e -> { e | count = operator e.count })
                |> Maybe.withDefault { card = card, count = operator 0 }
                |> Just

        updateDeck deck =
            deck
                |> Dict.update (getCardId card) applyOperation
                |> Dict.filter (\_ e -> (not << isEmpty) e)
    in
    updateDeck


makeCardOperator : (Int -> Int) -> (Deck -> MTGApi.Card -> Result DeckError Deck)
makeCardOperator operator =
    \deck card ->
        let
            modifier =
                makeEntryModifier operator card
        in
        deck
            |> checkAddConstraints operator card
            |> Result.map modifier


getManaCurve : Deck -> ManaCurve
getManaCurve deck =
    let
        getCurveCmc : DeckEntry -> Int
        getCurveCmc entry =
            if entry.card.cmc < 6 then
                entry.card.cmc

            else
                6

        sumCounts id entries =
            entries
                |> List.map .count
                |> List.sum

        curve =
            deck
                |> Dict.filter (\_ e -> not (isLand e.card))
                |> Dict.values
                |> Dict.Extra.groupBy getCurveCmc
                |> Dict.map sumCounts

        biggestCmc =
            curve
                |> Dict.values
                |> List.maximum
                |> Maybe.withDefault 1
    in
    { targetTotalCards = getCardCount deck
    , biggestCmc = biggestCmc
    , curve = curve
    }



-- Persistance


saveDeck : Deck -> Cmd msg
saveDeck deck =
    deck
        |> deckEncoder
        |> Encode.encode 0
        |> Ports.storeValue "deck"


deckEncoder : Deck -> Encode.Value
deckEncoder deck =
    deck
        |> Dict.toList
        |> List.map deckEntryEncoder
        |> Encode.object


deckEntryEncoder : ( CardId, DeckEntry ) -> ( CardId, Encode.Value )
deckEntryEncoder ( id, entry ) =
    ( id, Encode.int entry.count )
